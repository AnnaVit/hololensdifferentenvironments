# README #

Questo repository rappresenta il programma da poter installare ed eseguire su HoloLens 2, il quale consente di poter partecipare a un'esperienza condivisa stando in ambienti diversi, potendo vedere le manipolazioni effettuate dagli altri utenti, che possono accedere tramite un dispositivo Android o un dipositivo HoloLens 2.

### Premessa utilizzo della besh ###

Per poter utilizzare questo repository sul vostro PC, si è notato che l'utilizzo di bash come *GIT Bash* o *Windows Powershell* per salvare i file modificati nel repository, genera dei problemi a causa dei packages dell'MRTK che sono molti e presentano nomi troppo lunghi per queste besh, per questo motivo si è proceduto all'installazione e all'utilizzo di una *Linux besh*, che invece non ha dato questi porblemi ed è risultata essere anche abbastanza veloce nel salvataggio delle modifiche, se possedete un PC Windows potete trovare le istruzioni su come ottenere la bash al seguente link: https://docs.microsoft.com/it-it/windows/wsl/install-win10 oppure se siete in difficoltà, potete seguire questo tutorial https://www.youtube.com/watch?v=5W5EahK9ubo&t=412s.

### Istruzioni per l'esecuzione ###

 - Una volta importato il repository, aggiungere il progetto ad Unity Hub e procedere alla sua apertura.
 - Una volta che il porgetto è stato aperto correttamente, spostarsi nella scena principale, andando su **Assets/Scenes/** aprendo la scena *MainScene.unity*.
 - Aprire le impostazioni di progetto cliccando su **File** -> **Build Settings** a questo punto cambiare:

   - La piattaforma in *Universal Windows Platform* cliccando sul pulsante **Switch Platform** dopo la selezione,
   - Target Device in *HoloLens*,
   - Architecture in *ARM64*,
   - Build and Run on *USB Device*,
   - Build configuration *Relese*.

- Dalle impostazioni di Build cliccare sul pulsante **Player Settings**, andare sulla voce **XR Plug-in Management** e verificare che:

  - La piattaforma scelta sia *Universal Windows Platform*, quindi che l'icona selezionata sia quella di Windows,
  - La voce *OpenXR* sia selezionata così come la voce *Microsoft HoloLens feature set*,
  - Nel caso in cui affianco alla voce OpenXR, dovesse esserci un'icona rossa, cliccateci sopra, a questo punto dovrebbe aprirsi una dialog in cui appare un pulsante **Fix All** cliccateci sopra e al termine delle operazioni di fix l'icona dovrebbe essere diventata bianca.


- Dalla voce **XR Plug-in Management** spostarsi alla sottovoce **OpenXR**, verificare che la piattaforma scelta sia *Universal Windows Platform* e che il **Depth Submission Mode** sia impostato su *Depth 16 Bit*.

### Istruzioni per il deployment ###

- A questo punto andando in **File** -> **Build Settings** si può procedere alla build del porgetto cliccando sul pulsante **Build**, creando una nuova cartella **Build** all'interno del progetto dove poterlo salvare, ricordando che questa cartella non verra tracciata da git,
- Al termine della build spostatevi nella cartella creata e aprite il porgetto Visual Studio, collegate il vostro dispositivo HoloLens tramite il cavo USB-C al computer e nella finestra Visual Studio impostate:

  - Modalità *Relese*,
  - Architettura *ARM64*,
  - Deployment *Dispositivo*.


- Per caricare il porgramma su HoloLens 2 a questo punto si può procedere in 2 modi:

  1. Cliccare su  *Debug* e la voce *Avvia senza eseguire il Debug*, in questo caso HoloLens deve rimanere acceso e l'applicaizone al termine della distibuzione si avvierà immediatamente sul dispositivo.
  2. Cliccare su *Compilazione* e la voce *Deploy solution*, in questo caso il device può anche essere spento e al termine del deployment, si potrà trovare l'applicazione su HoloLens con il nome *HoloLensApp*, che può essere modificato spostandosi in Unity andando su **Build Settings** -> **Player Settings** alla voce **Player** selezionando il menu **Icon**, cercando la voce **Short Name**.


### Note da Ricordare ###

L'indirizzo del server viene immesso all'avvio dell'applicazione dall'utente, tramite l'apposita GUI che li viene presentata, se si sta utilizzando l'indirizzo IP privato del server allora, **client e server per poter comunicare devono essere connessi alla stessa rete**, verificare questo tramite le impostazioni di HoloLens.

Una volta che la connessione è andata a buon fine si potrà visualizzare l'ologramma e procedere alla sua manipolazione.
