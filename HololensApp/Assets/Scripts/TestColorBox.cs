using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestColorBox : MonoBehaviour
{

    [SerializeField]
    GameObject sphere;
    // Start is called before the first frame update
    void Start()
    {
        //sphere.GetComponent<DimBoxes.BoundBox>().wireColor = Color.red;
        sphere.GetComponent<DimBoxes.BoundBox>().lineColor = Color.blue;
        
        sphere.GetComponent<DimBoxes.BoundBox>().init();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
